package lab02;

import java.util.Arrays;
public class ex6 {
public static void main(String[] args){   
    int[] my_array1 = {
        1789, 2035, 1899, 1456, 2013, 
        1458, 2458, 1254, 1472, 2365, 
        1456, 2165, 1457, 2456};
     int sum=0;       
     
    System.out.println("Original numeric array : "+Arrays.toString(my_array1));
    Arrays.sort(my_array1);
    System.out.println("Sorted numeric array : "+Arrays.toString(my_array1));
    
    for (int i = 0; i < my_array1.length; i++) {
        sum=sum+ my_array1[i];
    }
    double ave = sum/my_array1.length;
    System.out.println("sum of the array: "+ sum);
    System.out.println("average of the array:"+ ave);
    }
}