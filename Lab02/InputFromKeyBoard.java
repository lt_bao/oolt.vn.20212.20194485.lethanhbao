package lab02;

import java.util.Scanner;

public class InputFromKeyBoard {
    public static void main(String[] args) {
        
        Scanner keyboard= new Scanner(System.in);

        System.out.println("what 's your name?");
        String strName= keyboard.nextLine();
        
        System.out.println("How old are you?");
        int age= keyboard.nextInt();

        System.out.println("How tall are you/");
        double height= keyboard.nextDouble();

        System.out.println("Mrs/Ms. "+ strName +" is "+ age+" years old. "+"Yor height is " + height);

        keyboard.close();
    }
    
}
