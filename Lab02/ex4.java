package lab02;

import java.util.Scanner;

public class ex4{
    public static void main(String[] args) {
        int n;
        Scanner height = new Scanner(System.in);
       
        System.out.println(" Nhap chieu cao tam giac: ");
        n= height.nextInt();
   
        for (int i = 1; i <= n; i++) {
            int k=0;
            for (int space = i; space <= n-1; space++) {
                System.out.print("  ");
            }
            while (k != 2 * i - 1) {
                System.out.print("* ");
                ++k;
             }
            System.out.print("\n");
        }
        height.close();
    }
}