package hust.soict.hedspi.aims.disc;

public class DigitalVideoDisc {

    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void copyDVD(DigitalVideoDisc dvd) {
        this.setTitle(dvd.getTitle());
        this.setCategory(dvd.getCategory());
        this.setDirector(dvd.getDirector());
        this.setLength(dvd.getLength());
        this.setCost(dvd.getCost());
    }

    public DigitalVideoDisc(String title) {
        super();
        this.title = title;
    }

    public DigitalVideoDisc(String title, String category) {
        this.title = title;
        this.category = category;
    }

    public DigitalVideoDisc(String title, String category, String director) {
        this.title = title;
        this.category = category;
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    public DigitalVideoDisc() {
        super();
    }

    public void toStringDVD() {
        System.out.println("DVD - " + title + " - " + category + " - " + director + " - " + length + ": " + cost + "$");
    }

    public boolean search(String title) {
        String parts[] = title.split(" ");
        int count = 0;
        for (int i = 0; i < parts.length; i++) {
            if (this.title.contains(parts[i])) {
                count++;
            }
        }if(count==parts.length) return true;
        return false;
    }
}
