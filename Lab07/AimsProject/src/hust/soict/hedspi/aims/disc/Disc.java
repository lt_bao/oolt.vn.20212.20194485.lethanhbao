package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Playable;
import java.util.Scanner;

public class Disc extends Media implements Playable{
    private String director;
    private int length;

    public Disc() {
    }

    public Disc(String director, int length) {
        this.director = director;
        this.length = length;
    }

    public Disc(String title, String category, float cost, String director, int length) {
        super(title, category, cost);
        this.director = director;
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public int getLength() {
        return length;
    }

    @Override
    public Media inputInformation() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("Disc Director: ");
        String director = sc.nextLine();
        System.out.println("Disc Length: ");
        int length = Integer.parseInt(sc.nextLine());
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        return new Disc(title, category, cost, director, length);
    }

    public String toString(){
        return "Disc - " + super.toString() + " - Director " + this.getDirector() + " - Length " + this.getLength();
    }

    @Override
    public void play() {
    }
}
