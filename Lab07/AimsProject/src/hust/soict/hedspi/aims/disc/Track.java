package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Playable;
import java.util.Scanner;

public class Track implements Playable {

    private String title;
    private int length;

    public Track() {
    }

    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }

    public Track inputInformation() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter title: ");
        String title = sc.next();
        System.out.println("Enter length: ");
        int length = sc.nextInt();
        return new Track(title, length);
    }

    public String getTitle() {
        return title;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Track: " + this.title + " - Length: " + this.length;
    }

    @Override
    public void play() {
        System.out.println("Playing Track: " + this.getTitle());
        System.out.println("Track length: " + this.getLength());
    }
}
