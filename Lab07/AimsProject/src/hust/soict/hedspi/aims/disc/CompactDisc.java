package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Playable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable{
    private String artist;
    private List<Track> listOfTracks = new ArrayList<Track>();

    public CompactDisc() {
    }

    public CompactDisc(String title, String category, float cost, String director, int length, String artist){
        super(title, category, cost, director, length);
        this.artist = artist;
    }
    
    public CompactDisc(String title, String category, float cost, String director, int length, String artist, List<Track> listOfTracks){
        super(title, category, cost, director, length);
        this.artist = artist;
        this.listOfTracks = listOfTracks;
    }

    public String getArtist() {
        return artist;
    }
    
    public void addTrack(String title, int length){
        listOfTracks.add(new Track(title, length));
    }
    
    public void addTrack(Track t){
        listOfTracks.add(t);
    }
    
    public void removeTrack(Track t){
        listOfTracks.remove(t);
    }
    
    public int getLength(){
        int length = 0;
        for(int i=0; i<listOfTracks.size(); i++){
            length += listOfTracks.get(i).getLength();
        }
        return length;
    }
    
    @Override
    public Media inputInformation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("CD Director: ");
        String director = sc.nextLine();
        System.out.println("CD Length: ");
        int length = Integer.parseInt(sc.nextLine());
        System.out.println("CD Artist: ");
        String artist = sc.nextLine();
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        System.out.println("The number of track: ");
        int n = sc.nextInt();
        List<Track> list = new ArrayList<Track>(n);
        for(int i=0; i<n; i++){
            Track t = new Track().inputInformation();
            list.add(t);
        }
        return new CompactDisc(title, category, cost, director, length, artist, list);
    }

    public String toString() {
        return "CD - " + super.toString() + " - Artist " + this.getArtist();
    }

    @Override
    public void play() {
        for(int i = 0; i < listOfTracks.size(); i++){
            listOfTracks.get(i).play();
        }
    }
}
