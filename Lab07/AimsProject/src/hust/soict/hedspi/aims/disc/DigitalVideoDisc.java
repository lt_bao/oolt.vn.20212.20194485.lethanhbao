package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Playable;
import java.util.Scanner;

public class DigitalVideoDisc extends Disc implements Playable{

    public DigitalVideoDisc() {
    }

    public DigitalVideoDisc(String title, String category, float cost, String director, int length) {
        super(title, category, cost, director, length);
    }

    @Override
    public Media inputInformation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("DVD Director: ");
        String director = sc.nextLine();
        System.out.println("DVD Length: ");
        int length = Integer.parseInt(sc.nextLine());
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        return new DigitalVideoDisc(title, category, cost, director, length);
    }

    @Override
    public String toString() {
        return "DVD - " + super.toString();
    }

    public boolean search(String title) {
        String parts[] = title.split(" ");
        int count = 0;
        for (int i = 0; i < parts.length; i++) {
            if (this.getTitle().contains(parts[i])) {
                count++;
            }
        }if(count==parts.length) return true;
        return false;
    }
    
//    public void copyDVD(DigitalVideoDisc dvd){
//        this.setTitle(dvd.getTitle());
//        this.setDirector(dvd.getDirector());
//        this.setCategory(dvd.getCategory());
//        this.setLength(dvd.getLength());
//        this.setCost(dvd.getCost());
//    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }
}
