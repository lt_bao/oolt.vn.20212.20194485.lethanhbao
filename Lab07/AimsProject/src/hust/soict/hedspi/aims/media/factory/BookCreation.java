package hust.soict.hedspi.aims.media.factory;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.Media;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookCreation implements MediaCreation{

    @Override
    public Media createMediaFromConsole() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        System.out.println("Number of authors: ");
        int n = sc.nextInt();
        List<String> authors = new ArrayList<String>();
        for(int i=0; i<n; i++){
            System.out.println("Author: ");
            String author = sc.next();
            authors.add(author);
        }
        return new Book(title, category, cost, authors);
    }
    
}
