package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.factory.BookCreation;
import hust.soict.hedspi.aims.media.factory.CDCreation;
import hust.soict.hedspi.aims.media.factory.DVDCreation;
import hust.soict.hedspi.aims.media.factory.MediaCreation;
import hust.soict.hedspi.aims.media.factory.TrackCreation;
import hust.soict.hedspi.aims.order.Order;
import java.util.Scanner;

public class Aims {

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Back");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void showAdminMenu() {
        System.out.println("Product Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new item");
        System.out.println("2. Delete item by id");
        System.out.println("3. Display the items list");
        System.out.println("0. Back");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3");
    }

    public static Media createMedia(MediaCreation mc) {
        return mc.createMediaFromConsole();
    }

    public static void main(String[] args) {
        Order o1 = null;
        MediaCreation mc;

        int system;
        int chose;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("System");
            System.out.println("1. Admin");
            System.out.println("2. User");
            System.out.println("0. Exit");
            system = sc.nextInt();
            switch (system) {
                case 0:
                    System.out.println("Exit!");
                    System.exit(0);
                case 1:
                    do {
                        showAdminMenu();
                        int admin = sc.nextInt();
                        switch(admin){
                            case 1:
                                System.out.println("1. DVD");
                                System.out.println("2. CD");
                                System.out.println("3. Book");
                                System.out.println("4. Track");
                                System.out.println("0. Back");
                                System.out.println("Chose type of item:");
                                int c1 = sc.nextInt();
                                switch(c1){
                                    case 1:
                                        createMedia(new DVDCreation());
                                        break;
                                    case 2:
                                        createMedia(new CDCreation());
                                        break;
                                    case 3:
                                        createMedia(new BookCreation());
                                        break;
                                    case 4:
                                        createMedia(new TrackCreation());
                                        break;
                                    case 0:
                                    default:
                                        break;
                                }
                            case 2:
                                
                            case 3:
                                
                            
                            case 0:
                                break;
                            default:
                                break;
                        }
                    } while (true);
                case 2:
                    do {
                        showMenu();
                        chose = sc.nextInt();

                        switch (chose) {
                            case 1:
                                o1 = Order.createOrder();
                                System.out.println("Order has been created");
                                break;
                            case 2:
                                int media_type;
                                System.out.println("0. DVD");
                                System.out.println("1. CD");
                                System.out.println("2. Book");
                                System.out.println("Chose type of item:");
                                media_type = sc.nextInt();
                                switch (media_type) {
                                    case 0:
                                        o1.addMedia(media_type);
                                        System.out.println("If you want to play DVD");
                                        System.out.println("1. Yes");
                                        System.out.println("2. No");
                                        int a = sc.nextInt();
                                        if (a == 1) {
                                            o1.getNewOrderedItem().play();
                                        }
                                        break;
                                    case 1:
                                        o1.addMedia(media_type);
                                        System.out.println("If you want to play CD");
                                        System.out.println("1. Yes");
                                        System.out.println("2. No");
                                        int b = sc.nextInt();
                                        if (b == 1) {
                                            o1.getNewOrderedItem().play();
                                        }
                                        break;
                                    case 2:
                                        o1.addMedia(media_type);
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 3:
                                System.out.println("Id of item you want to delete: ");
                                int id = sc.nextInt();
                                o1.removeMedia(o1.getOrderItems().get(id));
                                break;
                            case 4:
                                o1.printOrder();
                                break;
                            case 0:
                                break;
                            default:
                                break;
                        }

                    } while (chose != 0);
            }
        } while (system != 0);

    }
}
