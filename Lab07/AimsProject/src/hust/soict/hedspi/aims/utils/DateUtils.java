package hust.soict.hedspi.aims.utils;

import hust.soict.hedspi.aims.utils.MyDate;

public class DateUtils {
    public static int Compare(MyDate date1, MyDate date2){
        if(date1.getYear() > date2.getYear()) return 1;// date1 > date2
        if(date1.getYear() < date2.getYear()) return -1;// date1 < date2
        if(date1.getMonth() > date2.getMonth()) return 1;
        if(date1.getMonth() < date2.getMonth()) return -1;
        if(date1.getDay() > date2.getDay()) return 1;
        if(date1.getDay() < date2.getDay()) return -1;
        return 0;                                       // date1 = date2        
    }
    
    public static MyDate[] Sorting(MyDate[] dates){
        for(int i = 0; i < dates.length; i++){
            for(int j = i + 1; j < dates.length; j++){
                if(Compare(dates[i], dates[j]) == 1){
                    MyDate temp = dates[i];
                    dates[i] = dates[j];
                    dates[j] = temp;
                }
            }
        }
        return dates;
    }
}
