package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.media.Media;
import java.util.Scanner;

public class CompactDisc extends Media{
    
    private String artist;
    private String director;

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
    
    @Override
    public Media inputInformation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("CD Artist: ");
        String artist = sc.nextLine();
        System.out.println("CD Director: ");
        String director = sc.nextLine();
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        return new CompactDisc(title, category, artist, director, cost);
    }

    public CompactDisc(String title, String category, String artist, String director, float cost) {
        super(title, category, cost);
        this.artist = artist;
        this.director = director;
    }

    public CompactDisc() {
        super();
    }
    
    @Override
    public String toString() {
        return "CD - " + super.toString() + " - Artist " + this.getArtist() + " - Director " + this.director;
    }
}
