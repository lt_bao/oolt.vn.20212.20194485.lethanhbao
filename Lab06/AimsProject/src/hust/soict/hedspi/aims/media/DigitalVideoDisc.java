package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.media.Media;
import java.util.Scanner;

public class DigitalVideoDisc extends Media{

    private String director;
    private int length;

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
    
    @Override
    public Media inputInformation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("DVD Director: ");
        String director = sc.nextLine();
        System.out.println("DVD Length: ");
        int length = Integer.parseInt(sc.nextLine());
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        return new DigitalVideoDisc(title, category, director, length, cost);
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category, cost);
        this.director = director;
        this.length = length;
    }
    
    public DigitalVideoDisc(String title){
        super(title);
    }

    public DigitalVideoDisc() {
    }

    @Override
    public String toString() {
        return "DVD - " + super.toString() + " - Director " + this.director + " - Length " + this.length;
    }

    public boolean search(String title) {
        String parts[] = title.split(" ");
        int count = 0;
        for (int i = 0; i < parts.length; i++) {
            if (this.getTitle().contains(parts[i])) {
                count++;
            }
        }if(count==parts.length) return true;
        return false;
    }
    
    public void copyDVD(DigitalVideoDisc dvd){
        this.setTitle(dvd.getTitle());
        this.setDirector(dvd.getDirector());
        this.setCategory(dvd.getCategory());
        this.setLength(dvd.getLength());
        this.setCost(dvd.getCost());
    }
}
