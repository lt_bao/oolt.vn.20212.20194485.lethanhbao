package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;

public class DiskTest {

    public static void main(String[] args) {
        Order o1 = Order.createOrder();
        DigitalVideoDisc dvdList[] = new DigitalVideoDisc[10];
        CompactDisc cdList[] = new CompactDisc[10];

        o1.addMedia(0);
        o1.addMedia(0);
        
        o1.addMedia(1);
        o1.addMedia(1);
        
        o1.addMedia(2);
        o1.printOrder(o1.getALuckyItem());
    }
}
