package hust.soict.hedspi.test.disc;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
        
        swap(jungleDVD, cinderellaDVD);
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
        System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());
        
        DigitalVideoDisc jungleDVD1 = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD1 = new DigitalVideoDisc("Cinderella");
        
        changeTitle(jungleDVD1, cinderellaDVD1.getTitle());
        System.out.println("jungle dvd title: " + jungleDVD1.getTitle());
    }
    
    public static void  swap(DigitalVideoDisc o1, DigitalVideoDisc o2){
        DigitalVideoDisc tmp = new DigitalVideoDisc();
        tmp.copyDVD(o1);
        o1.copyDVD(o2);
        o2.copyDVD(tmp);
    }
    
    public static void changeTitle(DigitalVideoDisc dvd, String title){
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
