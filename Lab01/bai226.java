import javax.swing.JOptionPane;
public class bai226 {
    public static void main(String[] args) {
    JOptionPane.showMessageDialog(null, "Giai phuong trinh bac 2 mot an", "Phan 1",JOptionPane.INFORMATION_MESSAGE);
       bac2motan();
    JOptionPane.showMessageDialog(null, "Giai phuong trinh bac nhat 1 an", "Phan 2", JOptionPane.INFORMATION_MESSAGE);
       bacnhat1an();
    JOptionPane.showMessageDialog(null, "Giai he phuong trinh bac nhat 2 an", "Phan 3", JOptionPane.INFORMATION_MESSAGE);
       bacnhat2an();
    }
    public static void bacnhat1an(){
        String a = JOptionPane.showInputDialog(null,"please input the fist number: ","Input the fist number",JOptionPane.INFORMATION_MESSAGE);
        String b = JOptionPane.showInputDialog(null,"please input the second number:","Input the second number",JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(a);
        double num2 = Double.parseDouble(b);
        if (num1 == 0) {
            JOptionPane.showMessageDialog(null,"Phuong trinh vo nghiem", "Ket qua",JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null,"Phuong trinh co nghiem duy nhat = " + -num2 / num1, "Ket qua",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public static void bacnhat2an(){
        String a11,a12,a21,a22,b1,b2;
        a11 = JOptionPane.showInputDialog(null,"please input a11: ","Input a11",JOptionPane.INFORMATION_MESSAGE);
        a12 = JOptionPane.showInputDialog(null,"please input a12: ","Input a12",JOptionPane.INFORMATION_MESSAGE);
        a21 = JOptionPane.showInputDialog(null,"please input a21: ","Input a21",JOptionPane.INFORMATION_MESSAGE);
        a22 = JOptionPane.showInputDialog(null,"please input a22: ","Input a22",JOptionPane.INFORMATION_MESSAGE);
        b1 = JOptionPane.showInputDialog(null,"please input b1: ","Input b1",JOptionPane.INFORMATION_MESSAGE);
        b2 = JOptionPane.showInputDialog(null,"please input b2: ","Input b2",JOptionPane.INFORMATION_MESSAGE);
        double num11 = Double.parseDouble(a11);
        double num12 = Double.parseDouble(a12);
        double num21 = Double.parseDouble(a21);
        double num22 = Double.parseDouble(a22);
        double num1 = Double.parseDouble(b1);
        double num2 = Double.parseDouble(b2);
        double D = num11 * num22 - num21 * num12;
        double D1 = num1 * num22 - num2 * num12;
        double D2 = num11 * num2 - num21 * num1;
        if(D==0){
            JOptionPane.showMessageDialog(null, "Phuong trinh co vo so nghiem", "Ket Qua", JOptionPane.INFORMATION_MESSAGE);
        }else if(D!=0){
            JOptionPane.showMessageDialog(null, "Phuong trinh co hai nghiem:"+D1/D+D2/D, "Ket Qua", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null, "Chuong trinh khong giai duoc", "Ket Qua", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static void bac2motan(){
        String num1,num2,num3;
        num1 = JOptionPane.showInputDialog(null, "please input a(a!=0): ","Input a");
        num2 = JOptionPane.showInputDialog(null, "please input b: ","Input b");
        num3 = JOptionPane.showInputDialog(null, "please input c: ","Input c");
        double a = Double.parseDouble(num1);
        double b = Double.parseDouble(num2);
        double c = Double.parseDouble(num3);
        if (a == 0){
            JOptionPane.showMessageDialog(null,"Nhap a!=0", "Say ra loi", JOptionPane.INFORMATION_MESSAGE);
        }else {
            double dental = b * b - 4 * a * c;
            if (dental == 0) {
                JOptionPane.showMessageDialog(null,"Phuong trinh co nghiem kep : x1 = x2 =  " + -b / (2 * a), "Ket Qua",JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null,"Phuong trinh co 2 nghiem la : x1 =  " 
                + ((-b + Math.sqrt(dental)) / (2 * a))+ "\nx2 = " + ((-b - Math.sqrt(dental)) / (2 * a)), "Ket Qua",JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }
}
