import java.util.Objects;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    private int quantity = 0;

    public DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public Order(){
    	for(int i = 0; i < MAX_NUMBERS_ORDERED; i++) {
            itemsOrdered[i] = null;
        }
    };


    
    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
    
        if(quantity >= MAX_NUMBERS_ORDERED) {
            System.out.println("This order is  greater than " + MAX_NUMBERS_ORDERED);
            return;
        }
        for(int i = 0; i < MAX_NUMBERS_ORDERED; i++) {
        
                itemsOrdered[i] = disc;
                quantity++;
                break;
            
        }
        System.out.println("Added to the order");
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        for(int i = 0; i < quantity; i++) {
            if(itemsOrdered[i].equals(disc)) {
                itemsOrdered[i] = null;
                quantity--;
                break;
            }
        }
    }
    
    public double getTotalCost() {
    	double cost = 0;
    	for(int i = 0; i < MAX_NUMBERS_ORDERED; i++) {
    		if(itemsOrdered[i] == null)
    			continue;
    		System.out.println(itemsOrdered[i].getCost());
    		cost += itemsOrdered[i].getCost();
    	}
    	return cost;
    }

}