package hust.soict.hedspi.test.utils;

import hust.soict.hedspi.aims.utils.DateUtils;
import hust.soict.hedspi.aims.utils.MyDate;
import static hust.soict.hedspi.aims.utils.DateUtils.Compare;

public class DateTest {
    public static void main(String[] args) {
        MyDate date1 = new MyDate(20, 2, 2022);
        MyDate date2 = new MyDate(20, 2, 2022);
        System.out.println(Compare(date1, date2));
        MyDate[] dates = new MyDate[5];
        dates[0] = new MyDate(11, 3, 2012);
        dates[1] = new MyDate(21, 9, 2010);
        dates[2] = new MyDate(7, 9, 2022);
        dates[3] = new MyDate(4, 5, 2005);
        dates[4] = new MyDate(3, 2, 2023);
        dates = DateUtils.Sorting(dates);
        for(int i=0; i<dates.length; i++){
            dates[i].print();
        }
    }
}
