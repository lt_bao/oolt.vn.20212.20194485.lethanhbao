package hust.soict.hedspi.test.media;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.disc.Track;
import java.util.Collection;
import java.util.List;

public class TestMediaCompareTo {
    public static void main(String[] args) {
        java.util.Collection collection = new java.util.ArrayList();
        
        DigitalVideoDisc dvd1 = new DigitalVideoDisc(0, "Star Wars", "", 12.34f, "", 124);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc(1, "The Lion King", "", 23.45f, "", 87);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc(2, "Aladdin", "", 34.56f, "", 90);
        
        //Add the dvd objects to the ArrayList
        collection.add(dvd2);
        System.out.println(dvd2.toString());
        collection.add(dvd1);
        System.out.println(dvd1.toString());
        collection.add(dvd3);
        System.out.println(dvd3.toString());
        
        //Iterate through the ArrayList and output their titles (unsorted order)
        java.util.Iterator iterator = collection.iterator();
        
        System.out.println("-----------------------------");
        System.out.println("The DVDs currently in the order are: ");
        
        while(iterator.hasNext()){
            System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
        }
        
        // Sort the collection of DVDs - based on the compareTo() method
        java.util.Collections.sort((java.util.List)collection);
        
        // Iterate through the ArrayList anh output their titles - in sorted order
        iterator = collection.iterator();
        
        System.out.println("-------------------------------");
        System.out.println("The DVD in sorted order are: ");
        while(iterator.hasNext()){
            System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
        }
        System.out.println("-------------------------------");
        
        
        java.util.Collection collection1 = new java.util.ArrayList();
        
//        List<Track> l1 = (List<Track>) new Track("1", 1);
//        CompactDisc cd1 = new CompactDisc(3, "BTS", "", 12.34f, "", 1, "", l1);
//        List<Track> l2 = (List<Track>) new Track("2", 2);
//        l2.add(new Track("3", 3));
//        CompactDisc cd2 = new CompactDisc(4, "IDM", "", 23.45f, "", 5, "", l2);
//        List<Track> l3 = (List<Track>) new Track("4", 10);
//        CompactDisc cd3 = new CompactDisc(5, "BlackPink", "", 34.56f, "", 10, "", l3);

        CompactDisc cd1 = (CompactDisc) new CompactDisc().inputInformation();
        CompactDisc cd2 = (CompactDisc) new CompactDisc().inputInformation();
        CompactDisc cd3 = (CompactDisc) new CompactDisc().inputInformation();
        
        //Add the cd objects to the ArrayList
        collection1.add(cd2);
        System.out.println(cd2.toString());
        collection1.add(cd1);
        System.out.println(cd1.toString());
        collection1.add(cd3);
        System.out.println(cd3.toString());
        
        //Iterate through the ArrayList and output their titles (unsorted order)
        java.util.Iterator iterator1 = collection1.iterator();
        
        System.out.println("-----------------------------");
        System.out.println("The CDs currently in the order are: ");
        
        while(iterator1.hasNext()){
            System.out.println(((CompactDisc)iterator1.next()).getTitle());
        }
        
        // Sort the collection of CDs - based on the compareTo() method
        java.util.Collections.sort((java.util.List)collection1);
        
        // Iterate through the ArrayList anh output their titles - in sorted order
        iterator1 = collection1.iterator();
        
        System.out.println("-------------------------------");
        System.out.println("The CD in sorted order are: ");
        while(iterator1.hasNext()){
            System.out.println(((CompactDisc)iterator1.next()).getTitle());
        }
        System.out.println("-------------------------------");
    }
}