package hust.soict.hedspi.aims.order;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.utils.MyDate;
import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.disc.Disc;
import hust.soict.hedspi.aims.media.Media;
import java.util.ArrayList;
import java.util.Scanner;

public class Order {

    public static final int MAXIMUM_ORDERED_ITEMS = 10;
    public static final int MAXIMUM_ORDERS = 5;
    public static final int MEDIA_DVD = 0;
    public static final int MEDIA_CD = 1;
    public static final int MEDIA_BOOK = 2;

    private static int nbOrders = 0;
    private MyDate dateOrdered;

    private int qtyOrdered = 0;

    private int nbOrderedItems;
//    private ArrayList <Media orderItems = new Media[MAXIMUM_ORDERED_ITEMS];
    private ArrayList<Media> orderItems = new ArrayList<Media>();
    
    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(MyDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }
    
    public ArrayList<Media> getOrderItems(){
        return this.orderItems;
    }

    private Order() {
        dateOrdered = new MyDate();
    }

    public static Order createOrder() {
        if (nbOrders >= MAXIMUM_ORDERS) {
            System.out.println("You have reached maximum orders");
            return null;
        } else {
            nbOrders++;
            return new Order();
        }
    }
    
    public Media createMedia(int media_type){
        if(media_type == MEDIA_DVD){
            return (new DigitalVideoDisc().inputInformation());
        }else if(media_type == MEDIA_CD){
            return (new CompactDisc().inputInformation());
        }else{
            return (new Book().inputInformation());
        }
    }
    
    public void addMedia(int media_type){
        orderItems.add(this.createMedia(media_type));
        qtyOrdered++;
    }
    
    public void addMedia(Media media){
        orderItems.add(media);
        qtyOrdered++;
    }
    
    public void removeMedia(Media item){
        for(int i=0; i < orderItems.size(); i++){
            if(orderItems.get(i) == item){
                qtyOrdered--;
                orderItems.remove(i);
            }
        }
    }
    
    public void showOrderInformation(){
        for(int i = 0; i < orderItems.size(); i++){
            System.out.println(orderItems.get(i));
        }
    }

    public float totalCost() {
        float sum = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            sum += orderItems.get(i).getCost();
        }
        return sum;
    }

    public float totalCost(Media item) {
        float sum = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            sum += orderItems.get(i).getCost();
        }
        sum -= item.getCost();
        return sum;
    }

    public void printOrder() {
        System.out.println("************************************Order********************************");
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered Item: ");
        for(int i=0; i<orderItems.size(); i++){
            System.out.println(orderItems.get(i).toString());
        }
        System.out.print("Total Cost: ");
        System.out.println(totalCost());
        System.out.println("****************************************************************************");
    }
    
    public void printOrder(Media item) {
        System.out.println("************************************Order********************************");
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered Item: ");
        for(int i=0; i<orderItems.size(); i++){
            System.out.println(orderItems.get(i).toString());
        }
        System.out.println("Specifying a lucky and free item");
        System.out.println(item.toString());
        System.out.print("Total Cost: ");
        System.out.println(totalCost(item));
        System.out.println("****************************************************************************");
    }

    public Media getALuckyItem() {
        int ramdomInt = (int) (Math.random() * qtyOrdered);
        return orderItems.get(ramdomInt);
    }
    
    public Disc getNewOrderedItem(){
        return (Disc) orderItems.get(orderItems.size()-1);
    }
}
