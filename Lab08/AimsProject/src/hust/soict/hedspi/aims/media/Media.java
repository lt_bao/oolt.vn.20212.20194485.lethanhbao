package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public abstract class Media implements Comparable<Object>{
    private int id;
    private String title;
    private String category;
    private float cost;

    public Media() {
        super();
    }

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String category) {
        this.title = title;
        this.category = category;
    }
    
    public Media(String title, String category, float cost) {
        super();
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    public Media(int id, String title, String category, float cost) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.cost = cost;
    }
    
//    public Media inputInformation(){
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Media Title: ");
//        String title = sc.nextLine();
//        System.out.println("Media Category: ");
//        String category = sc.nextLine();
//        System.out.println("Media Cost");
//        float cost = Float.parseFloat(sc.nextLine());
//        return new Media(title, category, cost);
//    }
    
    public abstract Media inputInformation();

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    public int getId() {
        return id;
    }
    
    @Override
    public String toString(){
        return  "ID " + this.id + " - Title " + this.title + " - Category " + this.category + " - Cost " + this.cost;
    }
    
    @Override
    public boolean equals(Object o){
        return (this.id == ((Media) o).getId());
    }

    @Override
    public int compareTo(Object o) {
        int a = this.getTitle().compareTo(((Media) o).getTitle());
        if(a>0) return 1;
        if(a<0) return -1;
        return 0;
    }
}
