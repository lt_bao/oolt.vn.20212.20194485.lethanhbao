package aimsproject;

import java.awt.datatransfer.DataFlavor;
import java.util.Scanner;

public class MyDate {

    private int day;
    private int month;
    private int year;
    
    public String coverMonth(int month){
        if(month == 1) return "January";
        if(month == 2) return "February";
        if(month == 3) return "March";
        if(month == 4) return "April";
        if(month == 5) return "May";
        if(month == 6) return "June";
        if(month == 7) return "July";
        if(month == 8) return "August";
        if(month == 9) return "September";
        if(month == 10) return "October";
        if(month == 11) return "November";
        if(month == 12) return "December";
        return "";
    }
    public String coverDay(int day){
        if(day == 1 || day == 31) return (String.valueOf(day)+"st");
        if(day == 2) return (String.valueOf(day)+"nd");
        if(day == 3) return (String.valueOf(day)+"rd");
        if(day >= 4 && day <= 30) return (String.valueOf(day)+"th");
        return "";
    }
    
    public int checkDay(String strDay) {
        if(strDay.equals("First") || strDay.equals("1st") || strDay.equals("1")) return 1;
        if(strDay.equals("Second") || strDay.equals("2nd") || strDay.equals("2")) return 2;
        if(strDay.equals("Third") || strDay.equals("3rd") || strDay.equals("3")) return 3;
        if(strDay.equals("Fourth") || strDay.equals("4th") || strDay.equals("4")) return 4;
        if(strDay.equals("Fifth") || strDay.equals("5th") || strDay.equals("5")) return 5;
        if(strDay.equals("Sixth") || strDay.equals("6th") || strDay.equals("6")) return 6;
        if(strDay.equals("Seventh") || strDay.equals("7th") || strDay.equals("7")) return 7;
        if(strDay.equals("Eighth") || strDay.equals("8th") || strDay.equals("8")) return 8;
        if(strDay.equals("Ninth") || strDay.equals("9th") || strDay.equals("9")) return 9;
        if(strDay.equals("Tenth") || strDay.equals("10th") || strDay.equals("10")) return 10;
        if(strDay.equals("Eleventh") || strDay.equals("11th") || strDay.equals("11")) return 11;
        if(strDay.equals("Twelfth") || strDay.equals("12th") || strDay.equals("12")) return 12;
        if(strDay.equals("Thirteenth") || strDay.equals("13th") || strDay.equals("13")) return 13;
        if(strDay.equals("Fourteenth") || strDay.equals("14th") || strDay.equals("14")) return 14;
        if(strDay.equals("Fifteenth") || strDay.equals("15th") || strDay.equals("15")) return 15;
        if(strDay.equals("Sixteenth") || strDay.equals("16th") || strDay.equals("16")) return 16;
        if(strDay.equals("Seventeenth") || strDay.equals("17th") || strDay.equals("17")) return 17;
        if(strDay.equals("Eighteenth") || strDay.equals("18th") || strDay.equals("18")) return 18;
        if(strDay.equals("Nineteenth") || strDay.equals("19th") || strDay.equals("19")) return 19;
        if(strDay.equals("Twentieth") || strDay.equals("20th") || strDay.equals("20")) return 20;
        if(strDay.equals("Twenty-first") || strDay.equals("21th") || strDay.equals("21")) return 21;
        if(strDay.equals("Twenty-second") || strDay.equals("22th") || strDay.equals("22")) return 22;
        if(strDay.equals("Twenty-thirt") || strDay.equals("23th") || strDay.equals("23")) return 23;
        if(strDay.equals("Twenty-fourth") || strDay.equals("24th") || strDay.equals("24")) return 24;
        if(strDay.equals("Twenty-fifth") || strDay.equals("25th") || strDay.equals("25")) return 25;
        if(strDay.equals("Twenty-sixth") || strDay.equals("26th") || strDay.equals("26")) return 26;
        if(strDay.equals("Twenty-seventh") || strDay.equals("27th") || strDay.equals("27")) return 27;
        if(strDay.equals("Twenty-eighth") || strDay.equals("28th") || strDay.equals("28")) return 28;
        if(strDay.equals("Twenty-ninth") || strDay.equals("29th") || strDay.equals("29")) return 29;
        if(strDay.equals("Thirtieth") || strDay.equals("30th") || strDay.equals("30")) return 30;
        if(strDay.equals("Thirty-first") || strDay.equals("31st") || strDay.equals("31")) return 31;
        return 0;
    }

    public int checkMonth(String strMonth) {
        if (strMonth.equals("January") || strMonth.equals("Jan.")
                || strMonth.equals("Jan") || strMonth.equals("1")) {
            return 1;
        }
        if (strMonth.equals("February") || strMonth.equals("Feb.")
                || strMonth.equals("Feb") || strMonth.equals("2")) {
            return 2;
        }
        if (strMonth.equals("March") || strMonth.equals("Mar.")
                || strMonth.equals("Mar") || strMonth.equals("3")) {
            return 3;
        }
        if (strMonth.equals("April") || strMonth.equals("Apr.")
                || strMonth.equals("Apr") || strMonth.equals("4")) {
            return 4;
        }
        if (strMonth.equals("May") || strMonth.equals("5")) {
            return 5;
        }
        if (strMonth.equals("June") || strMonth.equals("Jun")
                || strMonth.equals("6")) {
            return 6;
        }
        if (strMonth.equals("July") || strMonth.equals("Jul")
                || strMonth.equals("7")) {
            return 7;
        }
        if (strMonth.equals("August") || strMonth.equals("Aug.")
                || strMonth.equals("Aug") || strMonth.equals("8")) {
            return 8;
        }
        if (strMonth.equals("September") || strMonth.equals("Sept.")
                || strMonth.equals("Sep") || strMonth.equals("9")) {
            return 9;
        }
        if (strMonth.equals("October") || strMonth.equals("Oct.")
                || strMonth.equals("Oct") || strMonth.equals("10")) {
            return 10;
        }
        if (strMonth.equals("November") || strMonth.equals("Nov.")
                || strMonth.equals("Nov") || strMonth.equals("11")) {
            return 11;
        }
        if (strMonth.equals("December") || strMonth.equals("Dec.")
                || strMonth.equals("Dec") || strMonth.equals("12")) {
            return 12;
        }
        return 0;
    }

    public MyDate() {
        this.day = java.time.LocalDate.now().getDayOfMonth();
        this.month = java.time.LocalDate.now().getMonthValue();
        this.year = java.time.LocalDate.now().getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date) {
        String[] parts = date.split(" ");
        this.day = checkDay(parts[1]);
        this.month = checkMonth(parts[0]);
        this.year = Integer.parseInt(parts[2]);
    }
    
    public MyDate(String day, String month, String year){
        this.day = checkDay(day);
        this.month = checkMonth(month);
        this.year = Integer.parseInt(year);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day >= 1 && day <= 31) {
            this.day = day;
        } else {
            System.out.println("Day value is not correct!");
        }
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month >= 1 && month <= 12) {
            this.month = month;
        } else {
            System.out.println("Month value is not correct!");
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void accept() {
        String date;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhập vào chuỗi bất kỳ: ");
        date = scanner.nextLine();
        String[] parts = date.split(" ");
        this.day = checkDay(parts[1]);
        this.month = checkMonth(parts[0]);
        this.year = Integer.parseInt(parts[2]);
    }
    
    public void print(){
        String m = coverMonth(month);
        String d = coverDay(day);
        System.out.println(m + " " + d + " " + year);
    }
    
    public void printV2(){
        System.out.println(day + "/" + month + "/" + year);
    }
    
    public static void main(String[] args) {
        MyDate aDate1 = new MyDate();
        MyDate aDate2 = new MyDate(22, 3, 2012);
        MyDate aDate3 = new MyDate("Feb 13 2017");
        MyDate aDate = new MyDate("Fifteenth", "Feb", "2021");
        aDate.print();
        aDate1.print();
        aDate2.print();
        aDate3.print();
        System.out.println("Day   : " + aDate2.getDay());
        System.out.println("Month : " + aDate2.getMonth());
        System.out.println("Year  : " + aDate2.getYear());
        aDate3.setDay(20);
        aDate3.setMonth(5);
        aDate3.setYear(2022);
        aDate3.print();
        MyDate aDate4 = new MyDate();
        aDate4.accept();
        aDate4.print();
        aDate4.printV2();
        
    }

}
